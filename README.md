# eCommerce prices API service 


This is an example of a SpringBoot application providing a REST API. 

The application is build using [Maven](https://wikipedia.org/wiki/Maven), [Hexagonal Architecture](https://en.wikipedia.org/wiki/Hexagonal_architecture_(software)) and Spring Data for access to in memory [H2 database](https://wikipedia.org/wiki/H2_(DBMS)).


### Run the app
`mvn spring-boot:run`

### Run the test
`mvn clean compile test`
 
## REST API

This API is documented by [Swagger](https://wikipedia.org/wiki/Swagger_(software)) API reference wich can be accessed via url:

http://{host}:{port}/prices-api/swagger-ui/index.html

