package com.company.prices.application.mapper;

import com.company.prices.application.dto.PriceResponseDto;
import com.company.prices.domain.model.Price;

public class PriceMapperDomainDto {

    private PriceMapperDomainDto() {}

    public static PriceResponseDto mapToResponseDto(Price price) {
        PriceResponseDto priceResponseDto = new PriceResponseDto();

        if (price != null) {
            priceResponseDto = new PriceResponseDto(
                    price.getBrandId(),
                    price.getStartDate(),
                    price.getEndDate(),
                    price.getPriceList(),
                    price.getProductId(),
                    price.getPrice());
        }

        return priceResponseDto;
    }
}
