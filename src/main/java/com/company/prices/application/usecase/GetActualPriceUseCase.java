package com.company.prices.application.usecase;

import com.company.prices.application.dto.PriceResponseDto;
import com.company.prices.application.mapper.PriceMapperDomainDto;
import com.company.prices.domain.service.PriceService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class GetActualPriceUseCase {

    private final PriceService priceService;

    public GetActualPriceUseCase(PriceService priceService) {
        this.priceService = priceService;
    }

    public PriceResponseDto execute(LocalDateTime date, Long productId, Integer brandId) {
        return PriceMapperDomainDto.mapToResponseDto(
                priceService.getPriceByDateAndProductAndBrand(date, productId, brandId).orElseThrow());
    }

}
