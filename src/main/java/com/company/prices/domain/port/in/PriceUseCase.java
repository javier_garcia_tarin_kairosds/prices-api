package com.company.prices.domain.port.in;

import com.company.prices.domain.model.Price;

import java.time.LocalDateTime;
import java.util.Optional;

public interface PriceUseCase {

    Optional<Price> getPriceByDateAndProductAndBrand(LocalDateTime date, Long productId, Integer brandId);

}
