package com.company.prices.domain.port.out;

import com.company.prices.domain.model.Price;

import java.util.stream.Stream;

public interface PricePersintence {

    Stream<Price> getAllPrices();

}
