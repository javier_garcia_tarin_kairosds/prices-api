package com.company.prices.domain.service;

import com.company.prices.domain.model.Price;
import com.company.prices.domain.port.in.PriceUseCase;
import com.company.prices.domain.port.out.PricePersintence;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;

@Service
public class PriceService implements PriceUseCase {

    private final PricePersintence pricePersintence;

    public PriceService(PricePersintence pricePersintence) {
        this.pricePersintence = pricePersintence;
    }

    @Override
    public Optional<Price> getPriceByDateAndProductAndBrand(LocalDateTime date, Long productId, Integer brandId) {

        return pricePersintence.getAllPrices()
                .filter(price ->  (price.getBrandId().equals(brandId)
                                    && price.getProductId().equals(productId)
                                    && ((date.isAfter(price.getStartDate()) && date.isBefore(price.getEndDate()))
                                        || date.isEqual(price.getStartDate()) || date.isEqual(price.getEndDate())))
                ).max(Comparator.comparing(Price::getPriority));
    }
}
