package com.company.prices.infraestructure.jpa;

import com.company.prices.domain.model.Price;
import com.company.prices.domain.port.out.PricePersintence;
import com.company.prices.infraestructure.jpa.mapper.PriceMapperEntityDomain;
import com.company.prices.infraestructure.jpa.repository.PriceRepository;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class PriceJpa implements PricePersintence {

    private final PriceRepository priceRepository;

    public PriceJpa(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @Override
    public Stream<Price> getAllPrices() {
        return priceRepository.findAll().stream().map(PriceMapperEntityDomain::mapToDomain);
    }
}
