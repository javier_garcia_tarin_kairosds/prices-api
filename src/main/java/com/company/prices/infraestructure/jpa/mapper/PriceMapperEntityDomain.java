package com.company.prices.infraestructure.jpa.mapper;

import com.company.prices.domain.model.Price;
import com.company.prices.infraestructure.jpa.entity.PriceEntity;
import org.springframework.beans.BeanUtils;

public class PriceMapperEntityDomain {

    private PriceMapperEntityDomain() {}

    public static Price mapToDomain(PriceEntity priceEntity) {
        Price price = new Price();

        BeanUtils.copyProperties(priceEntity, price);

        return price;
    }

    public static PriceEntity mapToEntity(Price price) {
        PriceEntity priceEntity = new PriceEntity();

        BeanUtils.copyProperties(price, priceEntity);

        return priceEntity;
    }

}
