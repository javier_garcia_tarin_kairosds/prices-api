package com.company.prices.infraestructure.rest;

import com.company.prices.application.dto.PriceResponseDto;
import com.company.prices.application.usecase.GetActualPriceUseCase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

@Api("Endpoint for get actual price of product by brand")
@RestController
@RequestMapping("/prices")
public class PriceController {

    private final GetActualPriceUseCase getActualPriceUseCase;

    public PriceController(GetActualPriceUseCase getActualPriceUseCase) {
        this.getActualPriceUseCase = getActualPriceUseCase;
    }

    @ApiOperation(value = "Get actual price of brand's product in a concrete date")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping()
    public PriceResponseDto getPriceByProductAndBrandAndDate(
            @ApiParam("Date for consulting price") @RequestParam("date")
                @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime date,
            @ApiParam("Product id") @RequestParam("product") Long productId,
            @ApiParam("Brand id") @RequestParam("brand") Integer brandId) {

        try {
            return getActualPriceUseCase.execute(date, productId, brandId);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Price not found");
        }
    }

}
