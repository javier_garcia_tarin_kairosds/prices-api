DROP TABLE IF EXISTS prices;

CREATE TABLE prices (
    id INT PRIMARY KEY,
    brand_id INT,
    start_date TIMESTAMP,
    end_date TIMESTAMP,
    price_list INT,
    product_id INT,
    priority INT,
    price FLOAT ,
    curr VARCHAR(3)
);