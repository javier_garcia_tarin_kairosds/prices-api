package com.company.prices.application.usecase;

import com.company.prices.application.dto.PriceResponseDto;
import com.company.prices.domain.model.Price;
import com.company.prices.domain.port.out.PricePersintence;
import com.company.prices.domain.service.PriceService;
import com.company.prices.shared.domain.PriceMother;
import com.company.prices.shared.infraestructure.PriceResponseDtoMother;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GetActualPriceUseCaseTest {

    @Mock
    private PricePersintence pricePersintence;

    private GetActualPriceUseCase getActualPriceUseCase;

    @BeforeEach
    void init() {
        PriceService priceService = new PriceService(pricePersintence);
        getActualPriceUseCase = new GetActualPriceUseCase(priceService);
    }

    @Test
    void givenDateAndProductAndBrand_whenNoExistsPrice_thenThrowsExceptionTest() {
        Price price = PriceMother.dummy();
        PriceResponseDto responseExpected = PriceResponseDtoMother.dummy(price);
        LocalDateTime dateParam = LocalDateTime.of(2022, 2, 5, 12, 0, 0);

        when(pricePersintence.getAllPrices()).thenThrow(NoSuchElementException.class);

        assertThrows(NoSuchElementException.class,
                () -> getActualPriceUseCase.execute(dateParam, responseExpected.getProductId(), responseExpected.getBrandId()));
    }

    @Test
    void givenDateAndProductAndBrand_whenExistsOnePrice_thenReturnOnePriceTest() {
        Price price = PriceMother.dummy();
        PriceResponseDto responseExpected = PriceResponseDtoMother.dummy(price);
        LocalDateTime dateParam = LocalDateTime.of(2022, 2, 5, 12, 0, 0);

        when(pricePersintence.getAllPrices()).thenReturn(Stream.of(price));

        PriceResponseDto response = getActualPriceUseCase
                .execute(dateParam, responseExpected.getProductId(), responseExpected.getBrandId());

        verify(pricePersintence).getAllPrices();

        assertEquals(responseExpected, response);
    }

    @Test
    void givenDateAndProductAndBrand_whenExistsMultiplePrice_thenReturnHighPriorityPrice_Test() {
        Price price = PriceMother.dummy();
        price.setPriority(1);

        Price price2 = PriceMother.dummy();
        price2.setPriority(5);
        price2.setPrice(60.20F);

        PriceResponseDto responseExpected = PriceResponseDtoMother.dummy(price2);
        LocalDateTime dateParam = LocalDateTime.of(2022, 2, 5, 12, 0, 0);

        when(pricePersintence.getAllPrices()).thenReturn(Stream.of(price, price2));

        PriceResponseDto response = getActualPriceUseCase
                .execute(dateParam, responseExpected.getProductId(), responseExpected.getBrandId());

        verify(pricePersintence).getAllPrices();

        assertEquals(responseExpected, response);
    }

}
