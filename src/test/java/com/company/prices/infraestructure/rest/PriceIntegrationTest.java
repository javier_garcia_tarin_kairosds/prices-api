package com.company.prices.infraestructure.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
public class PriceIntegrationTest {

    private static final String PARAM_DATE = "date";
    private static final String PARAM_PRODUCT = "product";
    private static final String PARAM_BRAND = "brand";

    private static final String PRICE_BRAND_ID = "brandId";
    private static final String PRICE_START_DATE = "startDate";
    private static final String PRICE_END_DATE = "endDate";
    private static final String PRICE_PRICE_LIST = "priceList";
    private static final String PRICE_PRODUCT_ID = "productId";
    private static final String PRICE_PRICE = "price";

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;
    private DateTimeFormatter dateTimeFormatter;

    @BeforeEach
    void setUp(WebApplicationContext webApplicationContext) {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    }

    @Test
    public void givenDateAndProductAndBrand_whenExistsOnePrice_thenReturnOnePrice_Test1() throws Exception {
        mockMvc.perform(get("/prices")
                    .contentType(MediaType.APPLICATION_JSON)
                    .param(PARAM_DATE, "2020-06-14T10:00:00")
                    .param(PARAM_PRODUCT, "35455")
                    .param(PARAM_BRAND, "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(PRICE_BRAND_ID, is(1)))
                .andExpect(jsonPath(PRICE_START_DATE, is(LocalDateTime.of(2020, 6,14,0,0,0).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_END_DATE, is(LocalDateTime.of(2020, 12,31,23,59,59).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_PRICE_LIST, is(1)))
                .andExpect(jsonPath(PRICE_PRODUCT_ID, is(35455)))
                .andExpect(jsonPath(PRICE_PRICE, is(35.50)));
    }

    @Test
    public void givenDateAndProductAndBrand_whenExistsMultiplePrice_thenReturnHighPriorityPrice_Test2() throws Exception {
        mockMvc.perform(get("/prices")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param(PARAM_DATE, "2020-06-14T16:00:00")
                        .param(PARAM_PRODUCT, "35455")
                        .param(PARAM_BRAND, "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(PRICE_BRAND_ID, is(1)))
                .andExpect(jsonPath(PRICE_START_DATE, is(LocalDateTime.of(2020, 6,14,15,0,0).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_END_DATE, is(LocalDateTime.of(2020, 6,14,18,30,0).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_PRICE_LIST, is(2)))
                .andExpect(jsonPath(PRICE_PRODUCT_ID, is(35455)))
                .andExpect(jsonPath(PRICE_PRICE, is(25.45)));
    }

    @Test
    public void givenDateAndProductAndBrand_whenExistsOnePrice_thenReturnOnePrice_Test3() throws Exception {
        mockMvc.perform(get("/prices")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param(PARAM_DATE, "2020-06-14T21:00:00")
                        .param(PARAM_PRODUCT, "35455")
                        .param(PARAM_BRAND, "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(PRICE_BRAND_ID, is(1)))
                .andExpect(jsonPath(PRICE_START_DATE, is(LocalDateTime.of(2020, 6,14,0,0,0).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_END_DATE, is(LocalDateTime.of(2020, 12,31,23,59,59).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_PRICE_LIST, is(1)))
                .andExpect(jsonPath(PRICE_PRODUCT_ID, is(35455)))
                .andExpect(jsonPath(PRICE_PRICE, is(35.50)));
    }

    @Test
    public void givenDateAndProductAndBrand_whenExistsMultiplePrice_thenReturnHighPriorityPrice_Test4() throws Exception {
        mockMvc.perform(get("/prices")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param(PARAM_DATE, "2020-06-15T10:00:00")
                        .param(PARAM_PRODUCT, "35455")
                        .param(PARAM_BRAND, "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(PRICE_BRAND_ID, is(1)))
                .andExpect(jsonPath(PRICE_START_DATE, is(LocalDateTime.of(2020, 6,15,0,0,0).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_END_DATE, is(LocalDateTime.of(2020, 6,15,11,0,0).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_PRICE_LIST, is(3)))
                .andExpect(jsonPath(PRICE_PRODUCT_ID, is(35455)))
                .andExpect(jsonPath(PRICE_PRICE, is(30.50)));
    }

    @Test
    public void givenDateAndProductAndBrand_whenExistsMultiplePrice_thenReturnHighPriorityPrice_Test5() throws Exception {
        mockMvc.perform(get("/prices")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param(PARAM_DATE, "2020-06-16T21:00:00")
                        .param(PARAM_PRODUCT, "35455")
                        .param(PARAM_BRAND, "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(PRICE_BRAND_ID, is(1)))
                .andExpect(jsonPath(PRICE_START_DATE, is(LocalDateTime.of(2020, 6,15,16,0,0).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_END_DATE, is(LocalDateTime.of(2020, 12,31,23,59,59).format(dateTimeFormatter))))
                .andExpect(jsonPath(PRICE_PRICE_LIST, is(4)))
                .andExpect(jsonPath(PRICE_PRODUCT_ID, is(35455)))
                .andExpect(jsonPath(PRICE_PRICE, is(38.95)));
    }

}
