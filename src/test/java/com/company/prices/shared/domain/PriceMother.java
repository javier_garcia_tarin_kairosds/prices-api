package com.company.prices.shared.domain;

import com.company.prices.domain.model.Price;

import java.time.LocalDateTime;
import java.util.Currency;

public class PriceMother {

    public static Price dummy() {
        Price price  = new Price();
        price.setId(99L);
        price.setBrandId(1);
        price.setStartDate(LocalDateTime.of(2022, 2, 4, 10, 0, 0));
        price.setEndDate(LocalDateTime.of(2022, 2, 10, 10, 0, 0));
        price.setPriceList(3);
        price.setProductId(52364L);
        price.setPriority(1);
        price.setPrice(42.21F);
        price.setCurrency(Currency.getInstance("EUR"));

        return price;
    }

}
