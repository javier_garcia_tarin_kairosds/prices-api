package com.company.prices.shared.infraestructure;

import com.company.prices.application.dto.PriceResponseDto;
import com.company.prices.domain.model.Price;

public class PriceResponseDtoMother {

    public static PriceResponseDto dummy(Price price) {
        PriceResponseDto priceResponseDto = new PriceResponseDto();
        priceResponseDto.setBrandId(price.getBrandId());
        priceResponseDto.setStartDate(price.getStartDate());
        priceResponseDto.setEndDate(price.getEndDate());
        priceResponseDto.setPriceList(price.getPriceList());
        priceResponseDto.setProductId(price.getProductId());
        priceResponseDto.setPrice(price.getPrice());

        return priceResponseDto;
    }

}
